## The Problem
Our business is expanding, putting in more capital means more products, and effectively more customers. This brings about the need for a platform, on which products can be sold in an orderly and stress-free manner, leaving more time to other important aspects of managing the business.

---
## Scenarios

**1**Aisha launches the platform from her phone's web browser. She nnavigates to the womens section and selects a dress she likes, she activates the "quick view" option and realised her size isn't available. Then she picks another one, now she's excited because this one has her size. She picks her size and tries to add the product to her cart, then the system prompts her to login or create an account. As this is her first time on the platform she opts to create a new account. After the account opening process, she's redirected back to the product's page and now successfully adds it to her cart.

**2**Adekunle launches the platform with the intension of opening an account, however he sees an option to login with his google or facebook account and opts to use google instead of going through the stress of completing a form and trying to keep a new password in his head.

**3**Folashade 

---
## Requirements

---
## Target Environment

---
## Deliverables

---
## Client Acceptance Criteria